﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/vkn/consumed/e_consumed.html'
	, 'forms/vkn/objects/c_objects'
	, 'forms/vkn/consumed/x_consumed'
	, 'tpl!forms/vkn/consumed/v_consumed.xaml'
	, 'forms/base/codec/codec.tpl.xaml'
	, 'forms/vkn/base/h_profile'
	, 'tpl!forms/vkn/consumed/p_consumed.html'
	, 'forms/vkn/consumed/r_consumed'
],
function (c_binded, tpl, c_objects, x_consumed, print_tpl, codec_tpl_xaml, h_profile, profiled_tpl, r_consumed)
{
	return function ()
	{
		var controller = c_binded(tpl,
			{
				constraints: r_consumed()
				,field_spec:
					{
						Показания_по_объектам: { controller: c_objects }
					}
			});

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			$(sel + ' input[model_field_name="Контактный_телефон"]').inputmask(h_profile.phone_mask);
		}

		var x_consumed_instance = x_consumed();

		controller.BuildXamlView = function ()
		{
			var model = !this.binding ? this.model : base_GetFormContent.call(this);
			var content = print_tpl({
				form: model
				, PrepareChars: function (num, txt)
				{
					var res= [];
					for (var i= 0; i<num; i++)
						res.push((!txt || null==txt || i>=txt.length) ? '' : txt.charAt(i));
					return res;
				}
				, PrepareNumChars: function(num, num_to_txt)
				{
					txt = '' + num_to_txt;
					while (txt.length < num)
						txt = '0' + txt;
					return this.PrepareChars(num, txt);
				}
				, PrepareConsumed: function(txt)
				{
					var parts = null == txt ? [] : txt.split(',');
					var p0 = !parts || 0 >= parts.length ? '' : parts[0];
					while (p0.length < 7)
						p0 = ' ' + p0;
					p0 = p0.substring(0, 7);
					var p1 = !parts || 1 >= parts.length ? '' : parts[1];
					while (p1.length < 2)
						p1 = p1 + ' ';
					p1 = p1.substring(0, 2);
					return this.PrepareChars(9, p0 + p1);
				}
				, PrepareMonth: function(month)
				{
					var txt = h_profile.month_num_by_name(month);
					return this.PrepareChars(2,txt);
				}
				, PreparePhone: function(phone)
				{
					return this.PrepareChars(10, x_consumed_instance.EncodePhone(phone));
				}
			});
			var codec = codec_tpl_xaml();
			return codec.Encode(content);
		}

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return profiled_tpl({ form: this.model, month_name_by_num: h_profile.month_name_by_num });
		}

		controller.UseProfile = function (profile)
		{
			if (profile)
			{
				if (!this.model)
					this.model = {};
				if (!this.model.Абонент)
					this.model.Абонент = {};
				if (!this.model.Отчёт)
					this.model.Отчёт = {};
				if (profile.Abonent)
				{
					var abonent = profile.Abonent;
					var Абонент = this.model.Абонент;
					Абонент.НаименованиеАбонента = abonent.Name;
					Абонент.ЮрАдрес = h_profile.text_prepare_address(abonent.LegalAddress);
				}
				if (profile.User)
					this.model.Абонент.ПредставительАбонента = h_profile.text_report_fio(profile.User);
				if (profile.Report)
				{
					var report = profile.Report;
					var Отчёт = this.model.Отчёт;
					Отчёт.ВидДокумента = !report.Номер_корректировки ? '1' : '3';
					Отчёт.НомерКорректировки = !report.Номер_корректировки ? '' : report.Номер_корректировки;
					if (1 == Отчёт.НомерКорректировки.length)
						Отчёт.НомерКорректировки = '0' + Отчёт.НомерКорректировки;
					Отчёт.ПериодОтчета = !report.Период ? '' : 'Месяц'==report.Период ? '1' : 'Декада'==report.Период ? '2' : '';
					Отчёт.НомерДекады = !report.Декада ? '' : report.Декада;
					Отчёт.МесяцОтчета = !report.Месяц ? '' :h_profile.month_num_by_name(report.Месяц);
					Отчёт.ГодОтчета = !report.Год ? '' : report.Год;
				}
				if (this.binding)
					this.binding.model = this.model;
			}
		}

		controller.UseCodec(x_consumed_instance);

		return controller
	}
});
