﻿define([
	  'forms/vkn/consumed/c_consumed_ce'
	  , 'forms/vkn/consumed/ff_consumed'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'consumed'
		, Title: 'Информация о выполнении квоты для приёма на работу инвалидов'
		, FileFormat: FileFormat
	};
	return form_spec;
});
