﻿define([
	  'forms/base/codec/codec.xml'
	, 'forms/vkn/base/h_profile'
],
function (codec_xml, h_profile)
{
	return function ()
	{
		var xml_codec = codec_xml()
		xml_codec.schema =
			{
				tagName: 'Сведения'
				, fields:
					{
						'Данные':
							{
								type: 'array'
								, item:
									{
										tagName: 'Объект'
										, fields:
											{
												ДанныеПоСтокамГВС:
													{
														plain_array: true
													}
											}
									}
							}
					}
			};
		xml_codec.EncodeEmptyStringAsSingleTag = true;
		xml_codec.tabs = ['', '  ', '    ', '      ', '        ', '          ', '            ', '              ', '                ', '                  '];

		xml_codec.DecodePhone= function(txt_phone)
		{
			if (!txt_phone || null == txt_phone)
			{
				return txt_phone;
			}
			else
			{
				txt_phone = this.RemoveAllExceptNumbers(txt_phone);
				var pos_txt_phone = 0;
				var phone_mask = h_profile.phone_mask;
				var res = '';
				for (var i = 0; i < phone_mask.length; i++)
				{
					var c = phone_mask.charAt(i);
					if ('9' != c)
					{
						res += c;
					}
					else if (pos_txt_phone < txt_phone.length)
					{
						res += txt_phone.charAt(pos_txt_phone);
						pos_txt_phone++
					}
					else
					{
						res += '_';
					}
				}
				return res;
			}
		}

		xml_codec.RemoveAllExceptNumbers= function(txt)
		{
			var res = '';
			for (var i = 0; i < txt.length; i++)
			{
				var c = txt.charAt(i);
				if ('0' == c || '1' == c || '2' == c || '3' == c || '4' == c || '5' == c || '6' == c || '7' == c || '8' == c || '9' == c)
					res += c;
			}
			return res;
		}

		xml_codec.EncodePhone= function(txt_phone)
		{
			if (!txt_phone || null == txt_phone)
			{
				return txt_phone;
			}
			else
			{
				var res0 = 0 != txt_phone.indexOf('+7') ? txt_phone : txt_phone.substring(2, txt_phone.length);
				return this.RemoveAllExceptNumbers(res0).substring(0, 10);
			}
		}

		xml_codec.DecodeHeader= function(from,to)
		{
			if (from.Абонент)
			{
				var Абонент = from.Абонент;
				to.Контактное_лицо = Абонент.КонтактноеЛицо;
				to.Контактный_телефон = this.DecodePhone(Абонент.КонтактныйТелефон);
				to.Абонент = {
					Договор: Абонент.Договор
					, ДатаДоговора: Абонент.ДатаДоговора
					, НаименованиеАбонента: Абонент.НаименованиеАбонента
					, ЮрАдрес: Абонент.ЮрАдрес
					, ПредставительАбонента: Абонент.ПредставительАбонента
				};
			}

			if (from.Отчет)
			{
				var Отчет = from.Отчет;
				to.Дата_снятия_показаний = Отчет.ДатаПоказаний;
				to.Отчёт = {
					ВидДокумента: Отчет.ВидДокумента
					, НомерКорректировки: Отчет.НомерКорректировки
					, ПериодОтчета: Отчет.ПериодОтчета
					, НомерДекады: Отчет.НомерДекады
					, МесяцОтчета: Отчет.МесяцОтчета
					, ГодОтчета: Отчет.ГодОтчета
				};
			}
		}

		xml_codec.EncodeHeader = function (from)
		{
			var res = {
				Форма: 'BKH-K130'
				, Абонент: {
					Договор: !from.Абонент ? '' : from.Абонент.Договор
					, ДатаДоговора: !from.Абонент ? '' : from.Абонент.ДатаДоговора
					, НаименованиеАбонента: !from.Абонент ? '' : from.Абонент.НаименованиеАбонента
					, ЮрАдрес: !from.Абонент ? '' : from.Абонент.ЮрАдрес
					, КонтактноеЛицо: from.Контактное_лицо
					, КонтактныйТелефон: this.EncodePhone(from.Контактный_телефон)
					, ПредставительАбонента: !from.Абонент ? '' : from.Абонент.ПредставительАбонента
				}
				, Отчет: {
					ВидДокумента: !from.Отчёт ? '' : from.Отчёт.ВидДокумента
					, НомерКорректировки: !from.Отчёт ? '' : from.Отчёт.НомерКорректировки
					, ПериодОтчета: !from.Отчёт ? '' : from.Отчёт.ПериодОтчета
					, НомерДекады: !from.Отчёт ? '' : from.Отчёт.НомерДекады
					, МесяцОтчета: !from.Отчёт ? '' : from.Отчёт.МесяцОтчета
					, ГодОтчета: !from.Отчёт ? '' : from.Отчёт.ГодОтчета
					, ЧислоОбъектов: (!from.Показания_по_объектам) ? '0' : from.Показания_по_объектам.length + ''
					, ДатаПоказаний: from.Дата_снятия_показаний
				}
			};
			return res;
		}

		var periods = ['I', 'II', 'III', 'IV'];

		xml_codec.DecodeObject= function(from)
		{
			var to =
				{
					Объект:
						{
							Наименование: from.НаименованиеОбъекта
							, Код_точки_учёта: from.КодТочкиУчета
							, Адрес: from.АдресОбъекта
						}
					, Показания_ПУ:
						{
							за_текщий_период: from.ПоказанияТекущие
							, по_состоянию_на_предыдущий_период: from.ПоказанияПредыдущие
						}
					, Примечания: from.Примечания
				};
			if (from.ДанныеПоСтокамГВС)
			{
				for (var j = 0; j < periods.length && j < from.ДанныеПоСтокамГВС.length; j++)
				{
					var from_Стоки = from.ДанныеПоСтокамГВС[j];
					if (!to.СтокиГВС)
						to.СтокиГВС = {};
					to.СтокиГВС[periods[j]] =
						{
							значение: from_Стоки.ПоказанияГВС
							, Год: from_Стоки.Год
							, Месяц: h_profile.month_name_by_num(from_Стоки.Месяц)
						};
				}
			}
			return to;
		}

		xml_codec.EncodeObject= function(from,i)
		{
			i = i + '';
			while (i.length < 3)
				i = '0' + i;
			var to = { НомерОбъекта: i };
			if (from.Объект)
			{
				var Объект = from.Объект;
				to.НаименованиеОбъекта = Объект.Наименование;
				to.АдресОбъекта = Объект.Адрес;
				to.КодТочкиУчета = Объект.Код_точки_учёта;
			}
			if (from.Показания_ПУ)
			{
				var Показания_ПУ = from.Показания_ПУ;
				to.ПоказанияТекущие = Показания_ПУ.за_текщий_период;
				to.ПоказанияПредыдущие = Показания_ПУ.по_состоянию_на_предыдущий_период;
			}
			if (from.СтокиГВС)
			{
				for (var j = 0; j < periods.length; j++)
				{
					var period= periods[j];
					if (from.СтокиГВС[period])
					{
						var СтокиГВС = from.СтокиГВС[period];
						if (!to.ДанныеПоСтокамГВС)
							to.ДанныеПоСтокамГВС = [];
						var to_СтокиГВС =
							{
								Месяц: h_profile.month_num_by_name(СтокиГВС.Месяц)
								, Год: СтокиГВС.Год
								, ПоказанияГВС: СтокиГВС.значение
							};
						to.ДанныеПоСтокамГВС.push(to_СтокиГВС);
					}
				}
			}
			to.Примечания = from.Примечания;
			return to;
		}

		var base_Decode = xml_codec.Decode;
		xml_codec.Decode= function(txt)
		{
			var first_char = txt.charAt(0);
			if ('{' == first_char)
			{
				return JSON.parse(txt);
			}
			else
			{
				var xml_decode_res = base_Decode.call(this, txt);
				var res = {};
				if (xml_decode_res)
				{
					this.DecodeHeader(xml_decode_res, res);

					if (xml_decode_res.Данные && xml_decode_res.Данные.length)
					{
						for (var i = 0; i < xml_decode_res.Данные.length; i++)
						{
							var obj = this.DecodeObject(xml_decode_res.Данные[i]);
							if (!res.Показания_по_объектам)
								res.Показания_по_объектам = [];
							res.Показания_по_объектам.push(obj);
						}
					}
				}
				return res;
			}
		}

		var base_Encode = xml_codec.Encode;
		xml_codec.Encode = function (data)
		{
			var res = this.EncodeHeader(data);
			if (data.Показания_по_объектам)
			{
				for (var i= 0; i<data.Показания_по_объектам.length; i++)
				{
					if (!res.Данные)
						res.Данные = [];
					var xml_obj = this.EncodeObject(data.Показания_по_объектам[i],i+1);
					res.Данные.push(xml_obj);
				}
			}

			return base_Encode.call(this, res);
		}

		return xml_codec;
	}
});
