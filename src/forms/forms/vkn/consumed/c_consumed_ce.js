﻿define([
	  'forms/vkn/econtent/c_econtent'
	, 'forms/vkn/consumed/c_consumed'
	, 'forms/vkn/consumed/ff_consumed'
],
function (BaseController, BaseFormController, formSpec)
{
	return function ()
	{
		var controller = BaseController(BaseFormController, formSpec);

		controller.GetCoveringLetterText = function ()
		{
			return 'Направляем документ\r\n' +
				'   "Сведения о количестве потребленной воды и объемах отведенных сточных вод"';
		}

		return controller;
	}
});