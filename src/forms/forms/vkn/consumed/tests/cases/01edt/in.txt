include ..\..\..\..\..\wbt.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions

wait_text "Показания по объектам"

shot_check_png ..\..\shots\01edt.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\test1.result.xml

wait_click_full_text "Редактировать отчёт"

wait_text "Показания по объектам"

shot_check_png ..\..\shots\01edt.png

exit