﻿define([
	'forms/base/h_constraints'
],
function (h_constraints)
{
	return function ()
	{
		var constraints =
		[
			  h_constraints.for_Field('Абонент.Договор', h_constraints.NotEmpty())
			, h_constraints.for_Field('Абонент.ДатаДоговора', h_constraints.NotEmpty())
			, h_constraints.for_Field('Контактный_телефон', h_constraints.checkPhone())
		];
		return constraints;
	}
});
