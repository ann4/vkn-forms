﻿define([],
function ()
{
	var file_format =
	{
		FilePrefix: 'ВКН-130'
		, FileExtension: 'xml'
		, Description: 'VKN. Сведения о потреблённой/отведённой воде'
	};
	return file_format;
});

