define
(
	[
		  'forms/collector'
		, 'txt!forms/vkn/bindications/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/vkn/objects/tests/contents/test1.json.txt'
		, 'txt!forms/vkn/objects/tests/contents/02sav_2.json.etalon.txt'
		, 'txt!forms/vkn/consumed/tests/contents/test1.json.txt'
		, 'txt!forms/vkn/consumed/tests/contents/02sav.etalon.xml'
		, 'txt!forms/vkn/consumed/tests/contents/sample.xml'
	],
	function (collect)
	{
		return collect([
		  'vkn_bindications_01sav'
		, 'vkn_objects_test1'
		, 'vkn_objects_02sav_2'
		, 'vkn_consumed_test1'
		, 'vkn_consumed_02sav'
		, 'vkn_consumed_sample'
		], Array.prototype.slice.call(arguments,1));
	}
);