﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/vkn/bindications/e_bindications.html'
	, 'forms/vkn/bindications/r_bindications'
],
function (c_binded, tpl, r_bindications)
{
	return function ()
	{
		var controller = c_binded(tpl, { constraints: r_bindications() });

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			$(sel + ' input[model_field_name="Объект.Наименование"]').focus();

			var self = this;
			$(sel + ' div.period input.indication').change(function () { self.FixReadonly(); });

			this.FixReadonly();
		}

		controller.FixReadonly= function()
		{
			var sel= this.binding.form_div_selector;
			for (var i= 1; i<4; i++)
			{
				var prev_period_sel = sel + ' div.period.p' + (i - 1);
				var txt = $(prev_period_sel + ' input.indication').val();
				var readonly = (!txt || null == txt || '' == txt)
				var period_sel = sel + ' div.period.p' + i;
				$(period_sel + ' input').attr('readonly', readonly);
				$(period_sel + ' select').attr('disabled', readonly);
			}
		}

		controller.Fix= function(СтокиГВС,pname,стоки)
		{
			if (СтокиГВС && СтокиГВС[pname])
			{
				var s = СтокиГВС[pname];
				if (s.значение && null != s.значение && '' != s.значение &&
					s.Месяц && null != s.Месяц && '' != s.Месяц &&
					s.Год && null != s.Год && '' != s.Год)
				{
					стоки.push(s);
				}
			}
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent= function()
		{
			var res = base_GetFormContent.call(this);
			if (res.СтокиГВС)
			{
				var стоки = [];
				this.Fix(res.СтокиГВС, 'I', стоки);
				this.Fix(res.СтокиГВС, 'II', стоки);
				this.Fix(res.СтокиГВС, 'III', стоки);
				this.Fix(res.СтокиГВС, 'IV', стоки);
				if (стоки.length == 0)
				{
					delete res.СтокиГВС;
				}
				else
				{
					res.СтокиГВС = { I: стоки[0] };
					if (стоки.length > 1)
						res.СтокиГВС.II = стоки[1];
					if (стоки.length > 2)
						res.СтокиГВС.III = стоки[2];
					if (стоки.length > 3)
						res.СтокиГВС.IV = стоки[3];
				}
			}
			return res;
		}

		return controller
	}
});
