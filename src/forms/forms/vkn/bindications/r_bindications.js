﻿define([
	'forms/base/h_constraints'
],
function (h_constraints)
{
	return function ()
	{
		var constraints =
		[
			  h_constraints.for_Field('Объект.Наименование', h_constraints.NotEmpty())
			, h_constraints.for_Field('Объект.Код_точки_учёта', h_constraints.NotEmpty())
			/*, h_constraints.for_Field('Показания_ПУ.за_текщий_период', h_constraints.NotEmpty())*/
		];
		return constraints;
	}
});
