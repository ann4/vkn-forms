define
(
	[
		  'forms/collector'
		, 'forms/vkn/bindications/c_bindications'
		, 'forms/vkn/objects/c_objects'
		, 'forms/vkn/consumed/c_consumed'
	],
	function (collect)
	{
		return collect([
		  'bindications'
		, 'objects'
		, 'consumed'
		], Array.prototype.slice.call(arguments,1));
	}
);