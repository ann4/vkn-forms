﻿define(
function ()
{
	var phone_mask = "+7(999)999-99-99";

	var text_report_fio = function (user)
	{
		var name = '';
		if (user.LastName)
			name += user.LastName;
		if (user.FirstName)
			name += ' ' + user.FirstName[0] + '.'
		if (user.MiddleName)
			name += user.MiddleName[0] + '.'
		return name;
	}

	var text_prepare_address= function(address)
	{
		var res = '';
		if (address && null != address)
		{
			var address_part_names = ['Country', 'ZipIndex', 'Province', 'Area', 'City', 'Locality', 'Street'];
			var append_address_part= function(txt,part)
			{
				if (part && null != part && '' != part)
				{
					if (0 < txt.length)
						txt += ', ';
					txt += part;
				}
				return txt;
			}
			for (var i = 0; i < address_part_names.length; i++)
			{
				var address_part_name = address_part_names[i];
				if (address[address_part_name])
					res = append_address_part(res, address[address_part_name]);
			}
				
			if (address.HouseNumber && null!=address.HouseNumber && ''!=address.HouseNumber)
				res = append_address_part(res, 'д.' + address.HouseNumber);
			if (address.HousingNumber && null != address.HousingNumber && '' != address.HousingNumber)
				res = append_address_part(res, 'корп.' + address.HousingNumber);
			if (address.FlatNumber && null != address.FlatNumber && '' != address.FlatNumber)
				res = append_address_part(res, 'кв.' + address.FlatNumber);
		}
		return res;
	}

	var monthes = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];

	var month_num_by_name = function (name)
	{
		if (!name || null == name)
		{
			return null;
		}
		else
		{
			var fixed_name = name.toLowerCase();
			for (var i = 0; i < monthes.length; i++)
			{
				var month = monthes[i];
				if (month == fixed_name)
				{
					var res = (i + 1) + '';
					if (2 > res.length)
						res = '0' + res;
					return res;
				}
			}
			return null;
		}
	}

	var month_name_by_num = function (num)
	{
		var inum = parseInt(num);
		var month = monthes[inum - 1];
		return month;
	}

	var res =
		{
			  text_report_fio: text_report_fio
			, text_prepare_address: text_prepare_address
			, month_num_by_name: month_num_by_name
			, month_name_by_num: month_name_by_num
			, phone_mask: phone_mask
		};
	return res;
});
