include ..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\bindications\tests\cases\in.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions
shot_check_png ..\..\shots\02sav_2.png

wait_click_text "Пивзавод"

check_stored_lines bindications_fields_1
shot_check_png ..\..\shots\03edt_1.png
js wbt_SetModelFieldValue("Объект.Наименование", "Пивоваренный завод");
wait_click_text "Отменить"

shot_check_png ..\..\shots\02sav_2.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02sav_2.json.result.txt
wait_click_full_text "Редактировать отчёт"

shot_check_png ..\..\shots\02sav_2.png

je $('div.cpw-form-vkn-objects tbody[collection-index="0"] button.delete').click();
shot_check_png ..\..\shots\03edt_del.png
wait_click_text "Нет"

shot_check_png ..\..\shots\02sav_2.png

je $('div.cpw-form-vkn-objects tbody[collection-index="0"] button.delete').click();
shot_check_png ..\..\shots\03edt_del.png
wait_click_text "Да"

shot_check_png ..\..\shots\03edt.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\03edt.json.result.txt

exit