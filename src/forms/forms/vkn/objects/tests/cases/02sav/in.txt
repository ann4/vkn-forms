include ..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\bindications\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Показания по объектам:"

shot_check_png ..\..\shots\00new.png

wait_click_text "Добавить показания по объекту"
play_stored_lines bindications_fields_1
wait_click_full_text "Сохранить"

shot_check_png ..\..\shots\02sav_1.png

wait_click_text "Добавить"
play_stored_lines bindications_fields_2
shot_check_png ..\..\shots\02sav_0.png
wait_click_full_text "Сохранить"
wait_text "№1"

shot_check_png ..\..\shots\02sav.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02sav.json.result.txt

wait_click_full_text "Редактировать отчёт"
shot_check_png ..\..\shots\02sav.png

wait_click_text "№1"
js wbt_SetModelFieldValue("Объект.Наименование", "Банно-прачечный комбинат");
wait_click_full_text "Сохранить"

shot_check_png ..\..\shots\02sav_2.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02sav_2.json.result.txt

exit