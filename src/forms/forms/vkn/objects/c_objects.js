﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/vkn/objects/e_objects.html'
	, 'forms/base/b_collection'
	, 'forms/vkn/bindications/c_bindications'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
],
function (c_binded, tpl, b_collection, c_bindications, h_msgbox, h_validation_msg)
{
	return function ()
	{
		var is_empty = function (v)
		{
			var res = (!v || '' == v);
			return res;
		}

		var BuildClassesForItem = function (model, i_item)
		{
			var res = '';
			if (model)
			{
				var item = model[i_item]
				if (is_empty(item.Примечания))
					res += ' no-comments';
				if (!item.СтокиГВС || !item.СтокиГВС.I || is_empty(item.СтокиГВС.I.значение))
					res += ' no-value1';
				if (!item.СтокиГВС || !item.СтокиГВС.II || is_empty(item.СтокиГВС.II.значение))
					res += ' no-value2';
				if (!item.СтокиГВС || !item.СтокиГВС.III || is_empty(item.СтокиГВС.III.значение))
					res += ' no-value3';
				if (!item.СтокиГВС || !item.СтокиГВС.IV || is_empty(item.СтокиГВС.IV.значение))
					res += ' no-value4';
			}
			return res;
		}

		var item_modal_controller = function (model_item, title, on_after_save)
		{
			var controller =
				{
					ShowModal: function (e)
					{
						var controller = c_bindications();
						controller.SetFormContent(model_item);
						var bname_Ok = 'Сохранить';
						h_msgbox.ShowModal({
							controller: controller
							, width: 620
							, height: 600
							, title: title
							, buttons: [bname_Ok, 'Отменить']
							, onclose: function (bname, dlg)
							{
								if (bname_Ok == bname)
								{
									h_validation_msg.IfOkWithValidateResult(controller.Validate(), function ()
									{
										on_after_save(controller.GetFormContent());
										$(dlg).dialog('close');
									});
									return false;
								}
							}
						});
					}
				};
			return controller;
		}

		var controller = c_binded(tpl
			,{
				  CreateBinding: b_collection
				, item_modal_controller: item_modal_controller
				, h_msgbox: h_msgbox
				, BuildClassesForItem: BuildClassesForItem
			});

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			$(sel + ' button.delete').button(/*{ icon: "ui-icon-circle-minus", iconPosition: { iconPositon: "end" } }*/);
			$(sel + ' button.add').button({ icon: "ui-icon-circle-plus", iconPosition: { iconPositon: "end" } });
		}

		return controller
	}
});
