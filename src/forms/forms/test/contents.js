define
(
	[
		  'forms/collector'
		, 'txt!forms/test/contents/1.txt'
		, 'txt!forms/test/contents/invalid.txt'
	],
	function (collect)
	{
		return collect([
		  '1'
		, 'invalid'
		], Array.prototype.slice.call(arguments,1));
	}
);