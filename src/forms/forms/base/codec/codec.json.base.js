define(function (BaseCodec, GetLogger)
{
	return function (BaseCodec)
	{
		var codec = BaseCodec;

		var base_Decode = codec.Decode;
		codec.Decode= function(text)
		{
			var first_char = text.charAt(0);
			if ('{' == first_char)
			{
				return JSON.parse(text);
			}
			else
			{
				return base_Decode.call(this,text);
			}
		}

		return codec;
	}
});
