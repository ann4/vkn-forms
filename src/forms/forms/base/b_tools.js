define(function ()
{
	return function (sel)
	{
		var binding = {form_div_selector:sel};

		binding.CheckFirstLevelFieldItem= function (item_with_value)
		{
			var root = $(this.form_div_selector);
			var count = 0;
			for (var parent = item_with_value.parent() ;
				 count < 1000 && parent && null != parent && !parent.is(root) ;
				 parent = parent.parent())
			{
				count++;
				var model_field_name = parent.attr('model_field_name');
				if (typeof model_field_name !== typeof undefined && model_field_name !== false)
					return false;
			}
			return true;
		}

		binding.SafeDoWithField= function (item_with_value, model, func_to_save)
		{
			var field_name = item_with_value.attr('model_field_name');
			if (field_name)
			{
				func_to_save.call(this, item_with_value, model, field_name);
			}
		}

		binding.SaveFieldValue= function (model, field_name, value)
		{
			var name_parts = field_name.split('.');
			for (var i = 0; i < name_parts.length - 1; i++)
			{
				var name_part = name_parts[i];
				if (!model[name_part])
					model[name_part] = {};
				model = model[name_part];
			}
			model[name_parts[name_parts.length - 1]] =
				(false == value || true == value) ? value : $.trim(value);
		}

		binding.GetValueFieldInputText= function (item_with_value)
		{
			var str_class = item_with_value.attr('class');
			if (str_class != null && str_class.indexOf('without_space') > -1)
				return item_with_value.val().replace(/\s+/g, '');
			return item_with_value.val();
		}

		binding.SaveToModel_Input_Text= function (item_with_value, model, field_name)
		{
			if (this.CheckFirstLevelFieldItem(item_with_value))
				this.SaveFieldValue(model, field_name, this.GetValueFieldInputText(item_with_value));
		}

		binding.SaveToModel_Input_Radio= function (item_with_value, model, field_name)
		{
			if (item_with_value.attr('checked') && this.CheckFirstLevelFieldItem(item_with_value))
				model[field_name] = item_with_value.val();
		}

		binding.SaveToModel_Input_Checkbox= function (item_with_value, model, field_name)
		{
			if (this.CheckFirstLevelFieldItem(item_with_value))
				this.SaveFieldValue(model, field_name, 'checked' == item_with_value.attr('checked'));
		}

		binding.SaveFieldsToModel_func= function (model, SaveToModel)
		{
			var self = this;
			return function (index) { self.SafeDoWithField($(this), model, SaveToModel); }
		}

		binding.SaveToModel_Input= function (item_with_value, model, field_name)
		{
			var type = item_with_value.attr('type');
			switch (type)
			{
				case 'radio':
					this.SaveToModel_Input_Radio(item_with_value, model, field_name);
					break;
				case 'checkbox':
					this.SaveToModel_Input_Checkbox(item_with_value, model, field_name);
					break;
				default:
					this.SaveToModel_Input_Text(item_with_value, model, field_name);
					break;
			}
		}

		binding.SaveToModel_Select= function (item_with_value, model, field_name)
		{
			if (this.CheckFirstLevelFieldItem(item_with_value))
			{
				if (!this.select2_fields || !this.select2_fields[field_name])
				{
					this.SaveFieldValue(model, field_name, item_with_value.val());
				}
				else
				{
					this.SaveFieldValue(model, field_name, item_with_value.select2('data'));
				}
			}
		}

		binding.SaveFieldsToModel= function (form_div_selector, model)
		{
			var self = this;
			$(form_div_selector + ' input').each(self.SaveFieldsToModel_func(model, self.SaveToModel_Input));
			$(form_div_selector + ' select').each(self.SaveFieldsToModel_func(model, self.SaveToModel_Select));
		}

		binding.get_model_field_value= function (model, field_name)
		{
			var name_parts = field_name.split('.');
			for (var i = 0; i < name_parts.length - 1; i++)
			{
				var name_part = name_parts[i];
				if (!model[name_part])
					return '';
				model = model[name_part];
			}
			var last_name_part = name_parts[name_parts.length - 1];
			return (!model[last_name_part] && false != model[last_name_part]) ? '' : model[last_name_part];
		}

		return binding;
	}
});
