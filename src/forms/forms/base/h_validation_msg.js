define([
	'forms/base/h_msgbox'
],
function (h_msgbox)
{
	var title_reject = 'Отказ сохранять содержимое формы из-за нарушенных органичений';
	var helper_res =
	{
		IfOkWithValidateResult: function (validation_result, on_validate_ok_func)
		{
			if (null == validation_result)
			{
				on_validate_ok_func();
			}
			else if ('string' == typeof validation_result)
			{
				h_msgbox.ShowModal({title: title_reject, html:  '<pre>' + validation_result + '</pre>', width: 800});
			}
			else if ('[object Array]' === Object.prototype.toString.call(validation_result))
			{
				this.IfOkWithValidateResultArray(validation_result, on_validate_ok_func);
			}
			else
			{
				h_msgbox.ShowModal({ title: title_reject, html: 'Валидация завершена каким то непредсказуемым образом', width: 800 });
			}
		}
		,IfOkWithValidateResultArray: function (validation_result, on_validate_ok_func)
		{
			var html = '';
			var invalid = false;
			var descriptions = '';
			for (var i = 0; i < validation_result.length; i++)
			{
				var validate_constraint = validation_result[i];
				if (false == validate_constraint.check_constraint_result)
					invalid = true;
				descriptions += '<li>' + validate_constraint.description + '</li>';
			}
			if (invalid)
			{
				h_msgbox.ShowModal({ title: title_reject, html: '<ul>' + descriptions + '</ul>', width: 800, id_div: 'cpw-form-validation-message' });
			}
			else
			{
				var btn_ok_title = "Да, сохранить";
				h_msgbox.ShowModal({
					title: 'Предупреждение перед сохранением содержимого формы'
					, html: '<ul>' + descriptions + '</ul><center>Сохранить содержимое формы?</center>'
					, width: 800
					, buttons: [btn_ok_title, "Нет, вернуться к редактированию"]
					, onclose: function (btn_title)
					{
						if (btn_title == btn_ok_title)
							on_validate_ok_func();
					}
				});
			}
		}
	};
	return helper_res;
});
